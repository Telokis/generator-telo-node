const Generator = require("yeoman-generator");

const OptionManager = require("./OptionManager");

module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);

        this.optionManager = new OptionManager(this);

        this.optionManager.registerModule("eslint", {
            default: true,
        });

        this.optionManager.registerModule("module-alias", {
            default: true,
        });

        this.optionManager.registerModule("babel", {
            default: false,
        });

        this.optionManager.registerOption("toto", {
            desc: "Very good option.",
            type: Boolean,
            default: false,
        });

        this.log(this.optionManager.modules());

        this.appName = this.determineAppname();
    }

    prompting() {
        return this.optionManager.promptModules();
    }

    writing() {}

    install() {}
};
