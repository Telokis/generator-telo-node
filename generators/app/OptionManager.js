module.exports = class OptionManager {
    constructor(generator) {
        this.generator = generator;
        this.options = {};
        this.moduleList = {};
        this.moduleDefinitions = {};
        this.oneModuleHasOnly = false;
    }

    registerModule(name, options) {
        let status = null;

        this.moduleDefinitions[name] = options;

        this.generator.option(name, {
            type: Boolean,
            desc: `Activates the '${name}' module without prompting.`,
        });

        const onlyOption = `only-${name}`;

        this.generator.option(onlyOption, {
            type: Boolean,
            desc: `Adds the '${name}' module to the only list.`,
        });

        const value = this.generator.options[name];
        const only = this.generator.options[onlyOption];

        if (typeof value !== "undefined") {
            status = value;
        }

        this.moduleList[name] = {
            status,
            only,
        };

        if (only) {
            this.oneModuleHasOnly = true;
        }
    }

    registerOption(name, options) {
        const defaultValue = options.default;
        let forceDefault = false;

        delete options.default;

        this.generator.option(name, options);

        if (typeof defaultValue !== "undefined") {
            const defaultOption = `${name}:default`;

            this.generator.option(defaultOption, {
                hide: options.hide,
                type: Boolean,
                default: false,
                desc: `Forces option '${name}' to its default value without prompting.`,
            });

            forceDefault = this.generator.options[defaultOption];
        }

        let value = this.generator.options[name];

        if (typeof value === "undefined" && forceDefault) {
            value = defaultValue;
        }

        this.options[name] = value;
    }

    value(name) {
        return this.options[name];
    }

    modules() {
        if (this.oneModuleHasOnly) {
            return Object.entries(this.moduleList).reduce((data, [module, { only }]) => {
                if (only) {
                    data[module] = true;
                }
                return data;
            }, {});
        }

        return Object.entries(this.moduleList).reduce((data, [module, { status }]) => {
            if (status !== false) {
                data[module] = status;
            }

            return data;
        }, {});
    }

    async promptModules() {
        if (this.oneModuleHasOnly) {
            return;
        }

        const prompts = Object.entries(this.moduleList).reduce((list, [module, { status }]) => {
            if (status === null) {
                const { message, default: defaultValue } = this.moduleDefinitions[module];

                list.push({
                    type: "confirm",
                    name: module,
                    message: message || `Would you like to enable the '${module}' module?`,
                    default: defaultValue,
                });
            }

            return list;
        }, []);

        const results = await this.generator.prompt(prompts);

        Object.entries(results).forEach(([module, value]) => {
            this.moduleList[module].status = value;
        });

        return this.modules();
    }
};
