const fs = require("fs");

const packageJson = JSON.parse(fs.readFileSync("./package.json", "utf8"));

module.exports = {
    parserOptions: {
        ecmaVersion: 8,
        sourceType: "module",
    },
    parser: "babel-eslint",
    extends: ["airbnb-base", "prettier", "plugin:jest/recommended"],
    plugins: ["prettier", "import", "jest"],
    overrides: [
        {
            files: packageJson.jest.testMatch,
            env: {
                jest: true,
            },
            rules: {
                "jest/consistent-test-it": "error",
                "jest/prefer-strict-equal": "error",
                "jest/prefer-to-be-null": "error",
                "jest/prefer-to-be-undefined": "error",
                "jest/valid-describe": "error",
            },
        },
    ],
    env: {
        node: true,
    },
    rules: {
        "prettier/prettier": "error",

        "import/no-dynamic-require": "off",
        "import/no-extraneous-dependencies": "off",
        "import/no-unresolved": "error",

        camelcase: "off",
        "class-methods-use-this": "off",
        "consistent-return": "off",
        curly: ["error", "all"],
        "global-require": "off",
        "no-console": "warn",
        "no-param-reassign": "off",
        "no-plusplus": "off",
        "no-return-await": "off",
        "no-shadow": [
            "error",
            {
                allow: ["done", "err"],
            },
        ],
        "object-shorthand": ["error", "always"],
        "no-underscore-dangle": "off",
        "prefer-const": [
            "error",
            {
                destructuring: "any",
                ignoreReadBeforeAssign: false,
            },
        ],
        "prefer-destructuring": [
            "error",
            {
                VariableDeclarator: {
                    object: true,
                },
            },
        ],
    },
};
