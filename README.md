# generator-telo-node [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Yeoman generator made to scaffold a NodeJS project with proper dev tools ready.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-telo-node using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-telo-node
```

Then generate your new project:

```bash
yo telo-node
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Telokis](https://gitlab.com/Telokis)


[npm-image]: https://badge.fury.io/js/generator-telo-node.svg
[npm-url]: https://npmjs.org/package/generator-telo-node
[travis-image]: https://travis-ci.org//generator-telo-node.svg?branch=master
[travis-url]: https://travis-ci.org//generator-telo-node
[daviddm-image]: https://david-dm.org//generator-telo-node.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-telo-node
